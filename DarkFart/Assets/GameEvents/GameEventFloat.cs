using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventFloat", menuName = "GameEvent/GameEventFloat")]
public class GameEventFloat : GameEvent<float> { }