using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Semilla", menuName = "Item/Materiales/Semilla")]
public class Semilla : Materiales, IActionable
{
    [SerializeField] private GameObject m_Prefab;
    [SerializeField] private Tile[] m_Tiles;
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        if (m_Tiles.Contains(t.GetTile<Tile>(t.WorldToCell(position + direction))))
        {
            GameObject g = Instantiate(m_Prefab);
            g.transform.position = t.GetCellCenterWorld(t.WorldToCell(position + direction));
            //TODO need to quit one object from the backpack
        }
    }
}
