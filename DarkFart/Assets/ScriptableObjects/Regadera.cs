using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Regadera", menuName = "Item/Equipable/Herramientas/Regadera")]
public class Regadera : Equipable, IActionable
{
    [SerializeField] private Tile m_TileTarget;
    [SerializeField] private Tile m_TileNew;
    [SerializeField] private Tile m_Water;
    [SerializeField] private GameEventFloat m_WasteEnergy;
    [SerializeField] private int MaxWater;
    public int maxWater
    {
        get
        {
            return MaxWater;
        }
        set
        {
            MaxWater = value;
        }
    }
    [SerializeField] private int water;
    public int Water
    {
        get
        {
            return water;
        }
        set
        {
            water = value;
        }
    }
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        Vector3Int target = t.WorldToCell(position + direction);

        if (t.GetTile<Tile>(target) == m_Water) {
            water = MaxWater;
        }else if (t.GetTile<Tile>(target) == m_TileTarget && water > 0)
        {
            water--;
            m_WasteEnergy.Raise(EnergiaQueGasta);
            t.SetTile(target, m_TileNew);
        }
    }
}
