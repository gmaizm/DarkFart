using Ink.UnityIntegration;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu]
[Serializable]
public class PersonalidadInfo : ScriptableObject
{
    [SerializeField]
    private List<FriendshipLevelInfo> levels;
    public List<FriendshipLevelInfo> Levels { get { return levels; } }
    [SerializeField] private DialogueInfo recruitmentDialogue;
    public DialogueInfo RecruitmentDialogue { get {  return recruitmentDialogue; } }
}

[Serializable]
public struct FriendshipLevelInfo
{
    public float maxFriendship;
    public List<DialogueInfo> dialogues;
}

[Serializable]
public struct DialogueInfo
{
    public DefaultAsset inkDialogueFile;
    public TextAsset dialogueTextAsset;
}
