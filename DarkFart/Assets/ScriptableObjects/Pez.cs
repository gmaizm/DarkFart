using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pez")]
public class Pez : ScriptableObject
{
    [SerializeField] 
    private Alimento pezAlimento;
    public Alimento PezAlimento
    {
        get
        {
            return pezAlimento;
        }
        set
        {
            pezAlimento = value;
        }
    }
    [SerializeField] 
    private float speed;
    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            speed = value;
        }
    }
    [SerializeField] 
    private float size;
    public float Size
    {
        get
        {
            return size;
        }
        set
        {
            size = value;
        }
    }
    [SerializeField] 
    private float startCatchAmount;
    public float StartCatchAmount
    {
        get
        {
            return startCatchAmount;
        }
        set
        {
            startCatchAmount = value;
        }
    }
    [SerializeField] 
    private float decreaseCatchAmount;
    public float DecreaseCatchAmount
    {
        get
        {
            return decreaseCatchAmount;
        }
        set
        {
            decreaseCatchAmount = value;
        }
    }
    [SerializeField] 
    private float fillAmount;
    public float FillAmount
    {
        get
        {
            return fillAmount;
        }
        set
        {
            fillAmount = value;
        }
    }
    [SerializeField] 
    private float unfillAmount;
    public float UnfillAmount
    {
        get
        {
            return unfillAmount;
        }
        set
        {
            unfillAmount = value;
        }
    }
}
