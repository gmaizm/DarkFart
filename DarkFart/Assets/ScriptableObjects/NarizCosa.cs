using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NarizCosa", menuName = "Animal/NarizCosa")]
public class NarizCosa : Animal
{
    [SerializeField]
    private float eggTime;
    public float EggTime
    {
        get
        {
            return eggTime;
        }
        set
        {
            eggTime = value;
        }
    }
    [SerializeField]
    private Alimento egg;
    public Alimento Egg
    {
        get
        {
            return egg;
        }
        set
        {
            egg = value;
        }
    }
}
