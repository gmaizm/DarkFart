using UnityEngine;
using UnityEngine.Tilemaps;
using static Backpack;

[CreateAssetMenu(fileName = "Cofre", menuName = "Item/Estructura/Cofre")]
public class Cofre : Estructura, IActionable
{
    [SerializeField]
    private ItemSlot[] m_ItemSlots = new ItemSlot[36];
    public ItemSlot[] Inventory
    {
        get
        {
            return m_ItemSlots;
        }
        set
        {
            m_ItemSlots = value;
        }
    }

    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        base.Action(position, direction, t);

    }
}
