using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Animal")]
[Serializable]
public class Animal : ScriptableObject
{
    [SerializeField]
    private string name;
    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }
    [SerializeField]
    private float speed;
    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            speed = value;
        }
    }
    [SerializeField]
    private float hp;
    public float HP
    {
        get
        {
            return hp;
        }
        set
        {
            hp = value;
        }
    }
    [SerializeField]
    private List<Item> drops;
    public List<Item> Drops
    {
        get
        {
            return drops;
        }
        set
        {
            drops = value;
        }

    }

    [SerializeField]
    private float probabilidadReproduccion;
    public float ProbabilidadReproduccion { get { return probabilidadReproduccion; } set { probabilidadReproduccion = value; } }
}
