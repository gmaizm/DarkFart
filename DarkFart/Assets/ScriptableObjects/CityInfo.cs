using System;
using UnityEngine;

[CreateAssetMenu]
public class CityInfo : ScriptableObject
{
    [SerializeField] private string cityName;
    public string CityName { get { return cityName; } }
    [SerializeField] private Vector2Int cityPerimeterLeftRight;
    public Vector2Int CityPerimeterLeftRight { get {  return cityPerimeterLeftRight; } }
    [SerializeField] private Vector2Int cityPerimeterDownUp;
    public Vector2Int CityPerimeterDownUp { get { return cityPerimeterDownUp; } }
    [SerializeField] private string[] npcNames;
    public string[] NpcNames { get { return npcNames; } }
    [SerializeField] private EnemySpawnInfo[] enemySpawnInfos;
    public EnemySpawnInfo[] EnemySpawnInfos { get {  return enemySpawnInfos; } }
    [SerializeField] private ItemSpawnInfo[] itemSpawnInfos;
    public ItemSpawnInfo[] ItemSpawnInfos { get { return itemSpawnInfos; } }
    [SerializeField] private NPCSpawnInfo[] npcSpawnInfos;
    public NPCSpawnInfo[] NPCSpawnInfos { get { return npcSpawnInfos; } }
}

[Serializable]
public struct EnemySpawnInfo
{
    public Enemy enemy;
    public int minAmountGroupEnemies;
    public int maxAmountGroupEnemies;
    public int groupAmount;

}

[Serializable]
public struct ItemSpawnInfo
{
    public Item item;
    public int spawnProbability;
    public int minAmountItems;
    public int maxAmountItems;
    public int minQuantity;
    public int maxQuantity;

}
[Serializable]
public struct NPCSpawnInfo
{
    public PersonalidadInfo personalidad;
    public int spawnProbability;
    public int minAmountNPCs;
    public int maxAmountNPCs;
    public TascaInfo[] tasques;

}

[Serializable]
public struct TascaInfo
{
    public enum Tasca { NONE, REPAIR_WALLS };
    public Tasca tasca;
    public int probability;
}

