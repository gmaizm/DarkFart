using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Arma : Equipable, IActionable
{
    [SerializeField]
    protected float dmg;
    public float Dmg
    {
        get
        {
            return dmg;
        }
        set
        {
            dmg = value;
        }
    }
    [SerializeField]
    protected float cooldown;
    public float Cooldown
    {
        get
        {
            return cooldown;
        }
        set
        {
            cooldown = value;
        }
    }
    [SerializeField]
    protected float probCritico;
    public float ProbCritico
    {
        get
        {
            return probCritico;
        }
        set
        {
            probCritico = value;
        }
    }
    [SerializeField]
    protected float areaAttack;
    public float AreaAttack
    {
        get
        {
            return areaAttack;
        }
        set
        {
            areaAttack = value;
        }
    }
    [SerializeField]
    public bool isAble;

    [SerializeField] private GameEventFloatArma m_GameEvent;
    [SerializeField] protected GameEventFloat m_WasteEnergy;
    public void Action(Vector2 position, Vector2 direction, Tilemap t)
    {
        if (!isAble) return;

        isAble = false;

        if (Random.Range(0, 101) < probCritico)
            m_GameEvent.Raise(dmg * 2, cooldown, areaAttack, this);
        else
            m_GameEvent.Raise(dmg, cooldown, areaAttack, this);

        m_WasteEnergy.Raise(EnergiaQueGasta);
    }
}
