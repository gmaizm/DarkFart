using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCookRecipe : MonoBehaviour
{
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private Image m_Image;
    [SerializeField]
    private TextMeshProUGUI m_Description;
    [SerializeField]
    private Sprite m_SpriteDefault;
    [SerializeField]
    private GameObject m_Button;

    [Header("Functionality")]
    [SerializeField]
    private GameObject m_DisplayIngredientParent;
    [SerializeField]
    private GameObject m_DisplayIngredinetPrefab;

    [Header("Controller")]
    [SerializeField]
    private CookController m_CookController;

    public void Load(CookBook.CookRecipe cook)
    {
        if (cook != null && cook.recipe.Result.Name != null && cook.unlocked)
        {
            m_Text.gameObject.SetActive(true);
            m_Image.gameObject.SetActive(true);
            m_Description.gameObject.SetActive(true);
            m_Button.gameObject.SetActive(true);
            m_Text.text = cook.recipe.Result.Name;
            m_Description.text = cook.recipe.Description;
            if (cook.recipe.Result.Sprite != null)
                m_Image.sprite = cook.recipe.Result.Sprite;
            else
                m_Image.sprite = m_SpriteDefault;

            m_Button.GetComponent<Button>().onClick.RemoveAllListeners();
            m_Button.GetComponent<Button>().onClick.AddListener(() => Cook(cook));
            RefreshIngredients(cook);
        }
        else
        {
            ClearDisplay();
            m_Text.gameObject.SetActive(false);
            m_Image.gameObject.SetActive(false);
            m_Description.gameObject.SetActive(false);
            m_Button.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        m_CookController = CookController.Instance;
    }

    private void ClearDisplay()
    {
        foreach (Transform child in m_DisplayIngredientParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay(CookBook.CookRecipe cook)
    {
        for (int i = 0; i < cook.recipe.IngredientsCraft.Count; i++)
        {
            GameObject displayedItem = Instantiate(m_DisplayIngredinetPrefab, m_DisplayIngredientParent.transform);
            displayedItem.GetComponent<DisplayIngridient>().Load(cook.recipe.IngredientsCraft[i], QuantityCalcule(cook.recipe.IngredientsCraft[i]));
        }
    }

    public void RefreshIngredients(CookBook.CookRecipe cook)
    {
        ClearDisplay();
        FillDisplay(cook);
    }

    public int QuantityCalcule(Receta.Ingredients ingredient)
    {
        int quantityItem = 0;
        for (int i = 0; i < InventarioController.Instance.getBackpack().ItemSlots2.Count; i++)
        {
            if (InventarioController.Instance.getBackpack().ItemSlots2[i].Item == ingredient.ingredient)
            {
                quantityItem += InventarioController.Instance.getBackpack().ItemSlots2[i].Amount;
            }
        }

        return quantityItem;
    }

    public bool QuantitySubstraction(Receta.Ingredients ingredient)
    {
        List<int> listInts = new List<int>();
        for (int i = 0; i < InventarioController.Instance.getBackpack().ItemSlots2.Count; i++)
        {
            if (InventarioController.Instance.getBackpack().ItemSlots2[i].Item == ingredient.ingredient)
            {
                listInts.Add(i);
            }
        }
        int quantity = ingredient.quantity;
        int quantityInBackpack = 0;
        int listQuantity = listInts.Count;
        int j = 0;
        //Debug.Log(listInts.Count);
        while (quantity > 0 && j < listQuantity)
        {
            quantityInBackpack = InventarioController.Instance.getBackpack().ItemSlots2[listInts[j]].Amount;
            //Debug.Log("Quantity Substraction: " + ingredient.ingredient + " - " + quantity + " - " + quantityInBackpack);
            InventarioController.Instance.getBackpack().RemoveAmountByPos(listInts[j], quantity);
            quantity -= quantityInBackpack;

            j++;
        }
        if (quantity == 0)
            return true;
        else
            return false;
    }

    private bool CanCook(Receta.Ingredients ingredient)
    {
        int quantityItem = QuantityCalcule(ingredient);
        if (quantityItem >= ingredient.quantity)
        {
            return true;
        }
        return false;
    }

    public void Cook(CookBook.CookRecipe cook)
    {
        bool cancraft = true;
        if (InventarioController.Instance.getBackpack().CountOccupiedSlots() < 36)
        {
            for (int i = 0; i < cook.recipe.IngredientsCraft.Count; i++)
            {
                if (cancraft)
                    cancraft = CanCook(cook.recipe.IngredientsCraft[i]);
            }
            if (cancraft)
            {
                if (m_CookController.Cook(cook.recipe.Result, cook.recipe.Water))
                {
                    for (int i = 0; i < cook.recipe.IngredientsCraft.Count; i++)
                    {
                        QuantitySubstraction(cook.recipe.IngredientsCraft[i]);
                    }
                    RefreshIngredients(cook);
                }
                else
                    Debug.Log("Faltan materiales 2");
            }
            else
                Debug.Log("Faltan materiales");
        }
        else
            Debug.Log("No ha espacio en la mochila");
        RefreshIngredients(cook);
    }
}
