using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadItemMove : MonoBehaviour
{
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private TextMeshProUGUI m_AmountText;
    [SerializeField]
    private Image m_Image;

    public void loadItem(string text, string amount, Sprite image)
    {
        m_Image.sprite = image;
        m_Text.text = text;  
        m_AmountText.text = amount;
        activate();
    }
    private void activate()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        m_Text.gameObject.SetActive(false);
    }

    public void desactivate()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
