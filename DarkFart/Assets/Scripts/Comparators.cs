using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComparatorX : MonoBehaviour, IComparer<BarreraInfo>
{
    public int Compare(BarreraInfo x, BarreraInfo y)
    {
        return (int) (x.pos.x - y.pos.x);
    }
}

public class ComparatorY : MonoBehaviour, IComparer<BarreraInfo>
{
    public int Compare(BarreraInfo x, BarreraInfo y)
    {
        return (int) (x.pos.y - y.pos.y);
    }
}
