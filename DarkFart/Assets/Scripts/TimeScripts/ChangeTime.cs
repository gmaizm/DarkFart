using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeTime : MonoBehaviour
{
    private float actualtime=0;

    private void Start()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = "Time: "+ TimeSpan.FromSeconds(actualtime);
    }

    public void UpdateTime(float time)
    {
        actualtime = toSeconds(time);
        gameObject.GetComponent<TextMeshProUGUI>().text = "Time: " + TimeSpan.FromSeconds(actualtime);
    }

    private float toSeconds(float time)
    {
        return time *= 3600;
    }

}
