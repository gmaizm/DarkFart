using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaControllerBoss : VidaController
{

    [Header("Change Fase HP")]
    private float hpFase2;
    private float hpFase3;
    [Tooltip("El porcentaje al que quieres que canvie a la fase 2")]
    [SerializeField] 
    private float changeFase2;
    [Tooltip("El porcentaje al que quieres que canvie a la fase 3")]
    [SerializeField] 
    private float changeFase3; 

    private void Awake()
    {
        hp = maxHp;
        hpFase2 = maxHp * (changeFase2 / 100);
        hpFase3 = maxHp * (changeFase3 / 100);
    }

    private void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.N))
        {
            hp -= 10;
            checkFase();
            Debug.Log($"Hp: {hp}");
        }
        */
        
    }

    private void checkFase()
    {
        if (hp <= 0)
        {
            gameObject.GetComponent<BossController>().Drop();
        }
        else if (hp <= hpFase3)
        {
            gameObject.GetComponent<BossController>().fase3();
        }
        else if (hp <= hpFase2)
        {
            gameObject.GetComponent<BossController>().fase2();
        }
        else
        {
            gameObject.GetComponent<BossController>().fase1();
        }
    }

    public new void LessHP(float h)
    {
        Debug.Log("Boss dice auch");
        base.LessHP(h);
        checkFase();

    }
}
