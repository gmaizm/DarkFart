using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MesaBehaviour : MonoBehaviour, Interactuable, EstructuraBehaviour
{
    [SerializeField] private CraftController m_CraftController;
    private Estructura estructura;
    public Estructura Estructura { get { return estructura; } set { estructura = value; } }
    private void Start()
    {
        m_CraftController = CraftController.Instance;
    }

    public void Interact()
    {
        m_CraftController.craftBookVisible();
    }
}
