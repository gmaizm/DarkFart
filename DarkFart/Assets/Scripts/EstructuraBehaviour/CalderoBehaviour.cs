using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalderoBehaviour : MonoBehaviour, Interactuable, EstructuraBehaviour
{
    [SerializeField]
    private CookController m_CookController;
    [SerializeField]
    private int m_water;
    [SerializeField]
    private int m_maxWater = 100;
    [SerializeField]
    private int m_caldero;
    [SerializeField]
    private Backpack.ItemSlot m_wood;
    [SerializeField]
    private Image m_ImageWood;
    [SerializeField]
    private TextMeshProUGUI m_Amount;
    [SerializeField]
    private string m_nameWood = "Madera";
    private Estructura estructura;
    public Estructura Estructura { get { return estructura; } set { estructura = value; } }

    private void Start()
    {
        m_CookController = CookController.Instance;
        m_caldero = m_CookController.addCaldero(this);
    }

    public void Interact()
    {
        m_CookController.cookBookVisible(m_caldero);
    }

    public int getWater()
    {
        return m_water;
    }
    public int getMaxWater()
    {
        return m_maxWater;
    }

    public void setWater(int w)
    {
        if(w > m_maxWater)
            m_water = m_maxWater;
        else
            m_water = w;
    }
    public void setMaxWater(int mW)
    {
        m_maxWater = mW;
    }

    public void setImage(Image woodImage)
    {
        m_ImageWood = woodImage;
    }

    public void setText(TextMeshProUGUI amount)
    {
        m_Amount = amount;
    }
    public Backpack.ItemSlot getWood()
    {
        return m_wood;
    }
    public int setWood(Backpack.ItemSlot wood)
    {
        if (wood.Item.Name == m_nameWood)
        {
            m_wood.Item = wood.Item;
            int m_woodAmount = 0;
            if (m_wood != null)
            {
                m_woodAmount = m_wood.Amount;
            }
            int amount = 10 - m_woodAmount;
            if (amount >= wood.Amount)
            {
                m_wood.Amount += wood.Amount;
                m_ImageWood.sprite = wood.Item.Sprite;
                m_Amount.text = m_wood.Amount.ToString();
                return 0;
            }
            else if (amount < wood.Amount)
            {
                m_wood.Amount += amount;
                wood.Amount -= amount;
            }
            m_ImageWood.sprite = wood.Item.Sprite;
            m_Amount.text = m_wood.Amount.ToString();
            return wood.Amount;
        }
        return wood.Amount;
    }

    public void Loadwood(Backpack.ItemSlot wood)
    {
        m_wood = wood;
    }

    public void sumWater(int extraWater)
    {
        m_water += extraWater;
        if (m_water > m_maxWater)
            m_water = m_maxWater;
    }

    public bool tryCook(int waterUsed)
    {
        if (m_wood.Item == null)
            return false;
        //Debug.Log(m_water + " - " + waterUsed + " = " + (m_water - waterUsed));
        if ((m_water - waterUsed) < 0 || (m_wood.Amount - 1) < 0)
            return false;
        m_water -= waterUsed;
        m_wood.Amount--;
        m_Amount.text = m_wood.Amount.ToString();
        m_CookController.getWaterDisplay().mostrar();
        return true;
    }

    public int mirarWater()
    {
        return m_water;
    }

    internal int mirarWaterMax()
    {
        return m_maxWater;
    }
}
