using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DamageInfo : MonoBehaviour
{
    public float damage;
    [SerializeField] private int[] LayersNotToDamage;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Maybe we don't have to check this if layer are well implemented
        if (LayersNotToDamage.Contains(collision.gameObject.layer)) return;

        if (collision.gameObject.TryGetComponent<VidaController>(out VidaController vida))
            vida.LessHP(damage);

        if (collision.gameObject.TryGetComponent<VidaControllerBoss>(out VidaControllerBoss vidaboss))
            vidaboss.LessHP(damage);
    }
}
