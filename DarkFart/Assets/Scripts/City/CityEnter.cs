using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityEnter : MonoBehaviour
{
    [SerializeField] private SceneController.SCENES sceneName;
    [SerializeField] private MapInfo MapInfo;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        MapInfo.Save();
        FindFirstObjectByType<MovimentPlayer>().transform.position = new Vector3(1, -18, 0);
        SceneController.Instance.ChangeScene(sceneName);
    }
}
