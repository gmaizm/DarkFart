using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntradaController : MonoBehaviour
{
    [SerializeField]
    private int idEntrada;
    [SerializeField]
    private int idTp;
    [SerializeField]
    private Vector3 posToTp;

    private void Start()
    {
        CityTpManager.Instance.AddEntradaToTpList(idEntrada, posToTp);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.transform.position = CityTpManager.Instance.GetTransformFromId(idTp);
    }

}
