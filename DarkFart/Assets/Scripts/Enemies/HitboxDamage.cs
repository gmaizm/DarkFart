using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxDamage : MonoBehaviour
{
    [SerializeField]
    private EnemyController controller;
    private float damage;
    private int critProb;

    private void Start()
    {
        damage = controller.EnemySO.Damage;
        critProb = controller.EnemySO.CriticProbability;
        GetComponent<BoxCollider2D>().size = controller.EnemySO.AttackArea;
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        int r = Random.Range(1, 101);
        float realDamage = damage;
        if (r <= critProb)
        {
            realDamage *= 2;
        }
        if (collision.TryGetComponent<VidaController>(out VidaController vida))
            vida.LessHP(realDamage);

    }
}
