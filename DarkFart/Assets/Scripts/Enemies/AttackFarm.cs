using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AttackFarm : MonoBehaviour
{
    private NavMeshAgent m_Agent;
    [SerializeField] private Vector3 centroGranja;
    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();   
    }
    private void Start()
    {
        GoToCenter();
    }

    public void GoToCenter()
    {
        if(m_Agent.enabled)
            m_Agent.SetDestination(centroGranja);
    }
}
