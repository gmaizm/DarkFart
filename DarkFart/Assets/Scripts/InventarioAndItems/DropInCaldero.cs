using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DropInCaldero : MonoBehaviour
{
    [SerializeField]
    private int m_itemAmount;

    [SerializeField]
    private CalderoBehaviour m_Caldero;

    [SerializeField]
    private Image m_ImageWood;
    [SerializeField]
    private TextMeshProUGUI m_Amount;

    public int Drop(Backpack.ItemSlot itemDropped)
    {
        //Debug.Log("Gisela pedofila");
        m_itemAmount = itemDropped.Amount;
        return setItemInCaldero(itemDropped);
    }

    private int setItemInCaldero(Backpack.ItemSlot wood)
    {
        m_Caldero.setImage(m_ImageWood);
        m_Caldero.setText(m_Amount);
        int amount = m_Caldero.setWood(wood);
        //m_Amount.text = (m_itemAmount - amount).ToString();
        return amount;
    }

    public void setCaldero(CalderoBehaviour caldero)
    {
        m_Caldero = caldero;
    }

}
