using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarizCosaController : MonoBehaviour
{
    [SerializeField]
    private NarizCosa narizCosasSO;
    [SerializeField]
    private GameObject itemInWorld;
    [SerializeField]
    private TimeManager timeManager;

    private float increment; 
    //private float secsIncrement;

    private float timeForEggLeft;

    public void Start()
    {
        timeForEggLeft = narizCosasSO.EggTime;
        if(timeManager == null)
            timeManager = TimeManager.Instance;
        increment = timeManager.Increment;
        //secsIncrement = timeManager.SecsIncrement;
    }

    public void CheckTime()
    {
        timeForEggLeft -= increment;
        if (timeForEggLeft <= 0)
        {
            timeForEggLeft = narizCosasSO.EggTime;
            DropEgg();
        }
    }

    private void DropEgg()
    {
        Item i = narizCosasSO.Egg;
        GameObject g = Instantiate(itemInWorld);
        g.GetComponent<ItemInWorld>().Inicia(i, transform.position);
    }

}
