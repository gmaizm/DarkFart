using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitieDie : MonoBehaviour, IDieable
{
    [SerializeField]
    private List<Item> drops;
    public List<Item> Drops
    {
        get { return drops; }
        set { drops = value; }
    }
    [SerializeField]
    private GameObject itemInWorld;
    public void Die()
    {
        Item i = drops[Random.Range(0, drops.Count)];
        GameObject g = Instantiate(itemInWorld);
        g.GetComponent<ItemInWorld>().Inicia(i, transform.position);

        Destroy(this.gameObject);
    }
}
