using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FishingBarController : MonoBehaviour
{
    private Image m_Image;
    [SerializeField]
    private float catchPoints;
    private void Awake()
    {
        m_Image = GetComponent<Image>();
    }
    private void Start()
    {
        m_Image.fillAmount = catchPoints;
    }

    public void FillBar(float amount)
    {
        catchPoints = amount;
        m_Image.fillAmount = catchPoints;
    }
}
